# Go tools
[![GoDoc](https://godoc.org/gitlab.com/stormblest/tools/go-tools?status.svg)](https://godoc.org/gitlab.com/stormblest/tools/go-tools)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stormblest/tools/go-tools)](https://goreportcard.com/report/gitlab.com/stormblest/tools/go-tools)
[![Build Status](https://travis-ci.com/stormblest/go-tools.svg?branch=master)](https://travis-ci.com/stormblest/go-tools)
[![Coverage Status](https://coveralls.io/repos/gitlab/stormblest/tools/go-tools/badge.svg?branch=master)](https://coveralls.io/stormblest/tools/go-tools?branch=master)

This repository includes various tools and extensions that seemed to be missing in the standard go library and well known open source projects.
Dependencies are managed with `dep`.

## Content
* `pkg/extensions`
  * `collections` - extensions for operations on collections
  * `net` - utilities to convert and operate on structures returned by the standard go's `net` package
  * `os` - improvements to go's `os` package, currently mostly about loading env vars with defaults
  * `strings` - to operate on slices of strings
  * `time` - a `refresher` utility to periodically execute a callback function
* `pkg/linux/command` - execute linux commands and capture full output info: stdOut, stdErr and exitCode
* `nettools` - various utilities that wrap basic Linux network configuration commands, including
  * `conntrack` - currently only supports removing entries from conntrack
  * `interfaces` - allows for abstract implementation of `net` package interface info
  * `iproute` - management of `ip route` route table entries and `ip rule` routing rules
  * `ipset` - management of `ipset`: sets and set entries
  * `iptables` - management of `iptables` rules based on unique rule comments

## Getting started - contributing
1. Clone this repository to `$GOPATH/src/gitlab.com/stormblest/tools/go-tools`
1. Everything ready, you can run unit tests: `go test  -cover ./pkg/...`
